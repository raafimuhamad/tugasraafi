import React from 'react';

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            pass: ''
        }

        this.mengisiEmail = this.mengisiEmail.bind(this);
        this.mengisiPassword = this.mengisiPassword.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    mengisiEmail(event) { this.setState({ email: event.target.value }); }
    mengisiPassword(event) { this.setState({ pass: event.target.value }); }
    handleSubmit(event) {
        if (this.state.email != '' && this.state.pass != '') {
            this.state.email == 'test@email.com' && this.state.pass == '12345' ?
                alert('Login Sukses') : alert('Login gagal, coba isi dengan email: test@email.com & password: 12345 ');
        } else {
            alert('Email dan Password tidak boleh kosong!');
        }
    }

    render() {
        return (
            <div className="App-header">
                <div className="pages">
                    <form onSubmit={this.handleSubmit}>
                    <h2>Login</h2><hr/>
                        <table>
                            <tr>
                                <td className="labs">Email </td>
                                <td><input type="email" onChange={this.mengisiEmail} className="isian"/></td>
                            </tr>
                            <tr>
                                <td />
                            </tr>
                            <tr>
                                <td className="labs"> Password </td>
                                <td><input type="password" onChange={this.mengisiPassword} className="isian"/></td>
                            </tr>
                            <tr>
                                <td />
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" value="Log In" className="ton pjg" /></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        );
    }

}

export default Login