import React from "react";
export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nama: '',
            email: '',
            pass: '',
            pass2: '',
            pekerjaan: 'Fullstack Developer'
        }

        this.menyimpanNama = this.menyimpanNama.bind(this);
        this.menyimpanEmail = this.menyimpanEmail.bind(this);
        this.menyimpanPassword = this.menyimpanPassword.bind(this);
        this.menyimpanPassword2 = this.menyimpanPassword2.bind(this);
        this.menyimpanPekerjaan = this.menyimpanPekerjaan.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    menyimpanNama(event) { this.setState({ nama: event.target.value }); }
    menyimpanEmail(event) { this.setState({ email: event.target.value }); }
    menyimpanPassword(event) { this.setState({ pass: event.target.value }); }
    menyimpanPassword2(event) { this.setState({ pass2: event.target.value }); }

    menyimpanPekerjaan(event) { this.setState({ pekerjaan: event.target.value }); }
    handleSubmit(event) {
        if (this.state.nama == '') {
            alert('Nama tidak boleh kosong!');
        } else if (this.state.email == '') {
            alert('Email tidak boleh kosong!');
        } else if (this.state.pass == '') {
            alert('Password tidak boleh kosong!');
        } else if (this.state.pass != this.state.pass2) {
            alert('Password tidak sama!');
        } else {
            alert('nama adalah : ' + this.state.nama+ ', email anda : ' + this.state.email + ', password anda : ' + this.state.pass + ', dan pekerjaan anda adalah ' + this.state.pekerjaan);
        }
    }

    render() {
        return (
            <div className="App-header">
                <div className="pages">
                    <form onSubmit={this.handleSubmit}>
                        <h2>Ayo Daftar!</h2>
                        <hr />
                        <table>
                            <tr>
                                <td><label className="labs">Nama </label></td>
                                <td><input type="text" onChange={this.menyimpanNama} className="isian" /></td>
                            </tr>
                            <tr>
                                <td><label className="labs">Email </label></td>
                                <td><input type="email" onChange={this.menyimpanEmail} className="isian"/></td>
                            </tr>
                            <tr>
                                <td><label className="labs">Password </label></td>
                                <td><input type="password" onChange={this.menyimpanPassword} className="isian"/></td>
                            </tr>
                            <tr>
                                <td><label className="labs">Ulangi Password </label></td>
                                <td><input type="password" onChange={this.menyimpanPassword2} className="isian"/></td>
                            </tr>
                            <tr>
                                <td><label className="labs">Pekerjaan </label></td>
                                <td>
                                    <select value={this.state.pekerjaan} onChange={this.menyimpanPekerjaan} className="opti" >
                                        <option value="Fullstack Developer">Fullstack Developer</option>
                                        <option value="Frontend Developer">Frontend Developer</option>
                                        <option value="Backend Developer">Backend Developer</option>
                                        <option value="UI/UX Designer">UI/UX Designer</option>
                                    </select>
                                </td>
                            </tr>
                            <tr><td></td>
                                <td><input type="submit" value="Submit" className="ton" />
                                    <input type="reset" value="Reset" className="ton"/></td>
                            </tr>
                        </table>
                    </form>

                </div>
            </div>
        );
    }
}